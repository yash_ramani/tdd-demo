// To parse this JSON data, do
//
//     final loginResModel = loginResModelFromJson(jsonString);

import 'dart:convert';

Model loginResModelFromJson(String str) =>
    Model.fromJson(json.decode(str));

String loginResModelToJson(Model data) => json.encode(data.toJson());

class Model {
  Model({
    this.authToken,
    this.self,
  });

  String? authToken;
  String? self;

  factory Model.fromJson(Map<String, dynamic> json) => Model(
        authToken: json["authToken"] == null ? null : json["authToken"],
        self: json["_self"] == null ? null : json["_self"],
      );

  Map<String, dynamic> toJson() => {
        "authToken": authToken == null ? null : authToken,
        "_self": self == null ? null : self,
      };
}
