// To parse this JSON data, do
//
//     final loginRequestModel = loginRequestModelFromJson(jsonString);

import 'dart:convert';

SignupRequestModel loginRequestModelFromJson(String str) =>
    SignupRequestModel.fromJson(json.decode(str));

String loginRequestModelToJson(SignupRequestModel data) =>
    json.encode(data.toJson());

class SignupRequestModel {
  SignupRequestModel({
    this.url,
    this.user,
    this.pass,
  });

  String? url;
  String? user;
  String? pass;

  factory SignupRequestModel.fromJson(Map<String, dynamic> json) =>
      SignupRequestModel(
        url: json["url"] == null ? null : json["url"],
        user: json["user"] == null ? null : json["user"],
        pass: json["pass"] == null ? null : json["pass"],
      );

  Map<String, dynamic> toJson() => {
        "url": url == null ? null : url,
        "user": user == null ? null : user,
        "pass": pass == null ? null : pass,
      };
}
