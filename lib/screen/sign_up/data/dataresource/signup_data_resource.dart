import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_req_model.dart';

import '../datamodel/log_in_res_model.dart';

abstract class SignupDataResource {
  Future<Model> getSignup({SignupRequestModel model});

}
