import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:tdd_architexture_demo/constants/error_case.dart';
import 'package:tdd_architexture_demo/network/dio_api_client.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_req_model.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/dataresource/signup_data_resource.dart';

import '../../../../constants/status_objects.dart';
import '../datamodel/log_in_res_model.dart';

class SignUpDataResourceIml extends SignupDataResource {
  Dio? client;
  Dio? client1;


  SignUpDataResourceIml() {
    client = DioApiClient().getUnauthenticatedJsonApiClient();
    client1 = DioApiClient().getAuthenticatedJsonApiClient();
  }

  @override
  Future<Model> getSignup({SignupRequestModel? model}) async {
    client!.options.baseUrl = model!.url!;
    /// Here we perform Api calling
    Response response;
    try {
      return Model();
    } on Failure catch (e) {
      throw e;
    } catch (e) {
      throw getCurrentFailure(e);
    }
  }
}
