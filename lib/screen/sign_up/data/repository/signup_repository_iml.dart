import 'package:dartz/dartz.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_req_model.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_res_model.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/dataresource/signup_data_resource.dart';
import 'package:tdd_architexture_demo/screen/sign_up/domain/repository/signup_repository.dart';

import '../../../../constants/status_objects.dart';

class SignupRepositoryIml extends SignupRepository {
  SignupDataResource? dataResource;
  Either<Failure, Model>? data;

  SignupRepositoryIml({this.dataResource});



  @override
  Future<Either<Failure, Model>> signup({SignupRequestModel? model}) async {
    await Task(() => dataResource!.getSignup(model: model!))
        .attempt()
        .mapLeftToFailure()
        .run()
        .then((value) => data = value as Either<Failure, Model>);

    return data!;
  }
}
