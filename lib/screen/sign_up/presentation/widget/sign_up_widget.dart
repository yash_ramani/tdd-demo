import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

Widget textField(
    {TextEditingController? controller, BuildContext? context, String? hintText, FocusNode? focusNode}) =>
    TextFormField(
      keyboardType: TextInputType.number,
      // textAlign: TextAlign.start,
      controller: controller,
      // style: commonTextStyle(color: black,height: 1),
      onEditingComplete: () {
        FocusScope.of(context!).unfocus();
      },
      onFieldSubmitted: (val) {
        if (controller!.text.isEmpty)
          Fluttertoast.showToast(
              msg:
              "Mobile no. can't be empty");
        else if (controller
            .text.length !=
            10)
          Fluttertoast.showToast(
              msg:
              "Please enter valid mobile number");
      },
      focusNode: focusNode,
      decoration: InputDecoration(
        hintText: hintText,
        border: InputBorder.none,
      ),
    );