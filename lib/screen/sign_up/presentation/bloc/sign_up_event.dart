import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_req_model.dart';

abstract class SignUpEvent {}

class SignupEvent extends SignUpEvent {
  SignupRequestModel? requestModel;

  SignupEvent({this.requestModel});
}
