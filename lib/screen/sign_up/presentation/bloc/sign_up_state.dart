import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_res_model.dart';

abstract class SignUpState {}

class SignUpInitState extends SignUpState {}

class LoadingBeginState extends SignUpState {}

class LoadingEndState extends SignUpState {}

class ErrorState extends SignUpState {
  String? error;

  ErrorState({this.error});
}

class SignupState extends SignUpState {
  Model? model;

  SignupState({this.model});
}
