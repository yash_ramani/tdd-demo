import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tdd_architexture_demo/screen/sign_up/domain/usecase/signup_use_case.dart';

import 'bloc.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignupUseCase? useCase;

  SignUpBloc({this.useCase}) : super(SignUpInitState());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignupEvent) {
      yield LoadingBeginState();
      var result =
          await useCase!(SingUpUseCaseParams(model: event.requestModel));
      yield LoadingEndState();
      yield result.fold((error) => ErrorState(error: error.errorMessage),
          (success) => SignupState(model: success));
    }
  }
}
