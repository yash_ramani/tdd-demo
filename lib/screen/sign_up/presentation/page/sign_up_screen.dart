import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tdd_architexture_demo/common/widget/button.dart';
import 'package:tdd_architexture_demo/common/widget/process_indicator.dart';
import 'package:tdd_architexture_demo/common/widget/space_and_divider.dart';
import 'package:tdd_architexture_demo/screen/sign_up/presentation/bloc/sign_up_bloc.dart';
import 'package:tdd_architexture_demo/screen/sign_up/presentation/bloc/sign_up_event.dart';
import 'package:tdd_architexture_demo/screen/sign_up/presentation/bloc/sign_up_state.dart';
import 'package:tdd_architexture_demo/screen/sign_up/presentation/widget/sign_up_widget.dart';
import 'package:tdd_architexture_demo/utils/color.dart';
import 'package:tdd_architexture_demo/utils/custom_painter.dart';
import 'package:tdd_architexture_demo/utils/screen_utils.dart';
import 'package:tdd_architexture_demo/utils/string.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  SignUpBloc bloc = SignUpBloc();
  String? errorMsg = "";

  // bool btnStatus = false;
  bool isLoading = false;
  FocusNode mobileFocus = FocusNode();
  TextEditingController mobileController = TextEditingController();

  @override
  void initState() {
    super.initState();
    bloc.add(SignupEvent());
  }

  @override
  void dispose() {
    super.dispose();
    mobileController.clear();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    Screen.setScreenSize(context);
    return WillPopScope(
      onWillPop: onWillPop,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: BlocListener(
            bloc: bloc,
            listener: (BuildContext context, state) {
              if (state is LoadingBeginState) isLoading = true;
              if (state is LoadingEndState) isLoading = false;
              if (state is ErrorState) {
                isLoading = false;
                Fluttertoast.showToast(msg: state.error!);
              }
              if (state is SignUpState) {
                print("all sucess");
              }
            },
            child: BlocBuilder(
              bloc: bloc,
              builder: (context, state) => Stack(
                children: [
                  SingleChildScrollView(
                    reverse: true,
                    child: Stack(
                      children: <Widget>[
                        ClipPath(
                          clipper: customShapeClass(),
                          child: Container(
                            height: 247,
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 6,
                                  offset: Offset(0, 3),
                                  color: customShapeShadow,
                                ),
                              ],
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.topRight,
                                colors: customShapeGradiant,
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            padding: EdgeInsets.only(
                              left: 27.0,
                              right: 27.0,
                              bottom: MediaQuery.of(context).viewInsets.bottom,
                            ),
                            margin: EdgeInsets.only(bottom: 30),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                verticalSpace(86),
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(height: 63, width: 230),
                                ),
                                verticalSpace(44),
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: 90,
                                    width: 90,
                                    child: Text("applogo"),
                                  ),
                                ),
                                verticalSpace(35),
                                Text(
                                  hello_there,
                                  style: TextStyle(color: black, fontSize: 25),
                                ),
                                verticalSpace(15),
                                Text(sign_up_txt,
                                    maxLines: 2,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: lightText,
                                      fontSize: 18,
                                    )),
                                verticalSpace(15),
                                Container(
                                  height: 50,
                                  decoration: BoxDecoration(
                                    color: white,
                                    borderRadius: BorderRadius.circular(30),
                                    border: Border.all(
                                        color: textFieldBorder, width: 1),
                                  ),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Center(
                                          child: TextField(
                                            textAlign: TextAlign.right,
                                            enabled: false,
                                            decoration: InputDecoration(
                                              hintText: "+91",
                                              border: InputBorder.none,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: verticalDivider(
                                            color: textFieldBorder,
                                            thickness: 1),
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: textField(
                                            context: context,
                                            controller: mobileController,
                                            focusNode: mobileFocus,
                                            hintText: "Enter Mobile",
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                verticalSpace(10),
                                Text(
                                  otp_txt,
                                  maxLines: 2,
                                  style:
                                      TextStyle(color: lightText, fontSize: 12),
                                  textAlign: TextAlign.start,
                                ),
                                verticalSpace(Screen.screenHeight * 0.1),
                                submitButton(
                                    icon: Icons.arrow_forward,
                                    onPress: () async {},
                                    title: sign_in,
                                    titleSize: 18),
                                submitButton(
                                    icon: Icons.arrow_forward,
                                    onPress: () {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                        builder: (_) => Container(),
                                      ));
                                    },
                                    title: sign_up,
                                    titleSize: 18),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (isLoading) progressIndicator(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit the app'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text(
                  'No',
                  style: TextStyle(color: primaryColor),
                ),
              ),
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: new Text(
                  'Yes',
                  style: TextStyle(color: primaryColor),
                ),
              ),
            ],
          ),
        )) ??
        false;
  }
}
