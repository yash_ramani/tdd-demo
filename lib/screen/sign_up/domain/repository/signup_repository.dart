import 'package:dartz/dartz.dart';
import 'package:tdd_architexture_demo/constants/status_objects.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_req_model.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_res_model.dart';

abstract class SignupRepository {
  Future<Either<Failure, Model>> signup({SignupRequestModel model});

}
