import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:tdd_architexture_demo/constants/status_objects.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_req_model.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/datamodel/log_in_res_model.dart';
import 'package:tdd_architexture_demo/screen/sign_up/domain/repository/signup_repository.dart';
import 'package:tdd_architexture_demo/usecase/usecase.dart';

class SignupUseCase extends UseCase<Model, SingUpUseCaseParams> {
  SignupRepository? repository;

  SignupUseCase({this.repository});

  @override
  Future<Either<Failure, Model>> call(SingUpUseCaseParams? params) async {
    return await repository!.signup(model: params!.model!);
  }
}

// ignore: must_be_immutable
class SingUpUseCaseParams extends Equatable {
  SignupRequestModel? model;

  SingUpUseCaseParams({this.model});

  @override
  List<Object> get props => [model!];
}
