import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tdd_architexture_demo/utils/color.dart';
import 'package:tdd_architexture_demo/utils/screen_utils.dart';

Widget progressIndicator() {
  return Container(
      height: Screen.screenHeight,
      width: Screen.screenWidth,
      color: transBlack,
      child: Center(
          child: CircularProgressIndicator(
        strokeWidth: 4.0,
        valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
      )));
}

Widget circularProgressIndicator = Center(
    child: CircularProgressIndicator(
  valueColor: AlwaysStoppedAnimation<Color>(primaryColor),
));
