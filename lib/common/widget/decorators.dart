import 'package:flutter/cupertino.dart';
import 'package:tdd_architexture_demo/utils/color.dart';

final commonShadow = BoxShadow(
  blurRadius: 8.0,
  offset: Offset(0, 8),
  color: iconsTextColor.withOpacity(.1),
);
