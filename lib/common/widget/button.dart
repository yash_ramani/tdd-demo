import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tdd_architexture_demo/utils/color.dart';

Widget submitButton(
        {String? title,
        IconData? icon,
        Function? onPress,
        double? titleSize,
        bool isValid = true}) =>
    RaisedButton(
      padding: EdgeInsets.symmetric(vertical: 8),
      color: black,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title!,
            style: TextStyle(
              fontSize: titleSize!,
              color: white,
              fontWeight: FontWeight.w300,
            ),
          ),
          SizedBox(width: 5),
        ],
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      onPressed: null,
    );

Widget textButton({String? label, Function? onTap}) {
  return InkWell(
    onTap: () {},
    child: Text(
      label!,
      style: TextStyle(color: iconsTextColor),
    ),
  );
}
