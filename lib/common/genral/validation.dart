import 'package:tdd_architexture_demo/utils/string.dart';

String emptyValidationMsg({String? emptyField}) {
  if (emptyField!.isEmpty)
    return requiredField;
  else
    return valid;
}

String otpValidationMsg({String? otp}) {
  if (otp!.isEmpty)
    return requiredField;
  else if (otp.length == 4)
    return valid;
  else
    return '$invalid OTP';
}

String pinValidationMsg({String? pin}) {
  if (pin!.isEmpty)
    return requiredField;
  else if (pin.length == 6)
    return valid;
  else
    return '$invalid Pin code';
}

String mobileValidationMsg({String? mobile}) {
  if (mobile!.isEmpty)
    return requiredField;
  else if (mobile.length == 10)
    return valid;
  else
    return '$invalid Mobile number';
}

bool mobileValid({String? mobile}) => mobile == valid ? true : false;

bool emptyValid({String? emptyField}) => emptyField == valid ? true : false;

bool emailValid({String? email}) => email == valid ? true : false;

bool otpValid({String? otp}) => otp == valid ? true : false;

bool pinValid({String? pin}) => pin == valid ? true : false;
