import 'package:dio/dio.dart';

class Resource<T> {
  T? data;
  String? error;

  Resource({this.data, this.error});

  static String giveBackError({DioErrorType? type}) {
    print("type->$type");
    String error = "";
    switch (type!) {
      case DioErrorType.cancel:
        error = "Request is Cancel";
        break;
      case DioErrorType.connectTimeout:
        error = "Request took to much time";
        break;
      case DioErrorType.receiveTimeout:
        error = "Receive took to much time";
        break;
      case DioErrorType.sendTimeout:
        error = "Server took to much time";
        break;
      case DioErrorType.response:
        error = "Incorrect status, such as 404, 503...";
        break;
      case DioErrorType.other:
        error = "Something went wrong";
        break;
    }
    return error;
  }
}
