// To parse this JSON data, do
//
//     final notificationModel = notificationModelFromJson(jsonString);

import 'dart:convert';

NotificationModel notificationModelFromJson(String str) =>
    NotificationModel.fromJson(json.decode(str));

String notificationModelToJson(NotificationModel data) =>
    json.encode(data.toJson());

class NotificationModel {
  NotificationModel({
    this.title,
    this.body,
    this.description,
    this.image,
    this.sound,
    this.item,
    this.itemDescription,
    this.location,
    this.createdAt,
    this.updatedAt,
    this.quoteId,
    this.clickAction,
  });

  String? title;
  String? body;
  String? description;
  String? image;
  bool? sound;
  String? item;
  String? itemDescription;
  String? location;
  DateTime? createdAt;
  DateTime? updatedAt;
  int? quoteId;
  String? clickAction;

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        title: json["title"] == null ? null : json["title"],
        body: json["body"] == null ? null : json["body"],
        description: json["description"] == null ? null : json["description"],
        image: json["image"] == null ? null : json["image"],
        sound: json["sound"] == null ? null : json["sound"],
        item: json["item"] == null ? null : json["item"],
        itemDescription:
            json["item_description"] == null ? null : json["item_description"],
        location: json["location"] == null ? null : json["location"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        quoteId: json["quote_id"] == null ? null : json["quote_id"],
        clickAction: json["click_action"] == null ? null : json["click_action"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "body": body == null ? null : body,
        "description": description == null ? null : description,
        "image": image == null ? null : image,
        "sound": sound == null ? null : sound,
        "item": item == null ? null : item,
        "item_description": itemDescription == null ? null : itemDescription,
        "location": location == null ? null : location,
        "created_at": createdAt == null ? null : createdAt?.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt?.toIso8601String(),
        "quote_id": quoteId == null ? null : quoteId,
        "click_action": clickAction == null ? null : clickAction,
      };
}
