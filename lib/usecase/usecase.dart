import 'package:dartz/dartz.dart';
import 'package:tdd_architexture_demo/constants/status_objects.dart';

abstract class UseCase<Type, Params> {
  Future<Either<Failure, Type>> call(Params params);
}
