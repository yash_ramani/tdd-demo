import 'package:flutter/material.dart';
import 'package:tdd_architexture_demo/screen/sign_up/presentation/page/sign_up_screen.dart';

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

setUpAll() async {
  WidgetsFlutterBinding.ensureInitialized();
}

void main() async {
  await setUpAll();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      home: SignUpScreen(),
    );
  }
}
