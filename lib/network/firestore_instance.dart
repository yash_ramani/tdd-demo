import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

import 'firebase_string.dart';

class FireStoreInstance {
  static CollectionReference? chatReference;
  static CollectionReference? msgReference;
  static Reference? storageReference;
  static FirebaseFirestore? dataBaseReference;

  FireStoreInstance() {
    dataBaseReference = FirebaseFirestore.instance;
    chatReference = dataBaseReference?.collection(coll_chat);
    msgReference = dataBaseReference?.collection(coll_message);
    storageReference = FirebaseStorage.instance.ref().child(folder_chat);
  }
}
