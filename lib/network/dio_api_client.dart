import 'package:dio/dio.dart';

typedef ApiClientCallback = void Function(int value);

class DioApiClient {
  static const int timeout = 60000;

  String? authToken;

  Dio? authenticatedJsonApiDio = Dio();
  Dio? unauthenticatedJsonApiDio = Dio();

  /// used to notify listener where login is required when refresh token is expired
 late ApiClientCallback listener;

  getAuthenticatedJsonApiClient() {
    // s1.getData();
    return authenticatedJsonApiDio!;
  }

  Dio getUnauthenticatedJsonApiClient() {
    return unauthenticatedJsonApiDio!;
  }

  DioApiClient() {
    var logger = LogInterceptor(
        request: true,
        requestBody: true,
        responseBody: true,
        requestHeader: true,
        responseHeader: false);

    var tokenInterceptor = InterceptorsWrapper(
        onRequest: (RequestOptions options, RequestInterceptorHandler handler) {
      print('bjnv');
      options.headers['authToken'] = '';
      options.headers['Cookie'] = '';
      options.headers['Host'] = '';

      handler.next(options);
    });

    var authOptions = BaseOptions(
        baseUrl: "https://abc.in",
        connectTimeout: timeout,
        receiveTimeout: timeout,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
          // 'authToken': '$tokenValue',
          // 'Cookie': 'sessionId=$tokenValue',
          // 'Host': ' $hostValue'
        },
        responseType: ResponseType.json);
    var unAuthOptions = BaseOptions(
        connectTimeout: timeout,
        receiveTimeout: timeout,
        headers: {
          'accept': 'application/json',
          'Content-Type': 'application/json',
        },
        responseType: ResponseType.json);

    authenticatedJsonApiDio = Dio(authOptions);
    unauthenticatedJsonApiDio = Dio(unAuthOptions);
    authenticatedJsonApiDio!.interceptors.add(tokenInterceptor);
    authenticatedJsonApiDio!.interceptors.add(logger);
    unauthenticatedJsonApiDio!.interceptors.add(logger);
  }
}
