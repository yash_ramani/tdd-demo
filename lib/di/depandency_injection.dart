import 'package:get_it/get_it.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/dataresource/signup_data_resource.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/dataresource/signup_data_resource_iml.dart';
import 'package:tdd_architexture_demo/screen/sign_up/data/repository/signup_repository_iml.dart';
import 'package:tdd_architexture_demo/screen/sign_up/domain/repository/signup_repository.dart';
import 'package:tdd_architexture_demo/screen/sign_up/domain/usecase/signup_use_case.dart';
import 'package:tdd_architexture_demo/screen/sign_up/presentation/bloc/bloc.dart';

final getIt = GetIt.instance;

Future<void> initGetServiceLocator() async {
  ///login
  //bloc
  getIt.registerFactory(() => SignUpBloc(useCase: getIt()));
  //usecase
  getIt.registerFactory(() => SignupUseCase(repository: getIt()));
  //repository
  getIt.registerLazySingleton<SignupRepository>(
      () => SignupRepositoryIml(dataResource: getIt()));
  //data resource
  getIt
      .registerLazySingleton<SignupDataResource>(() => SignUpDataResourceIml());
}
