import 'package:equatable/equatable.dart';
import 'package:dartz/dartz.dart';

abstract class Status extends Equatable {}

class Success extends Status {
  final String message;

  Success(this.message);

  @override
  List<Object> get props => [message];
}

// class Failure extends Status {
//   final String message;
//
//   Failure(this.message);
//
//   @override
//   List<Object> get props => [message];
// }

class Failure {
  final String? errorMessage;
  final dynamic error;
  final bool requireConfirmation;

  Failure({this.error, this.errorMessage, this.requireConfirmation = false});

  @override
  String toString() {
    return errorMessage!;
  }
}


