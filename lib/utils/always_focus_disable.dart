import 'package:flutter/cupertino.dart';

class AlwaysFocusDisable extends FocusNode {
  @override
  bool get hasFocus => false;
}
