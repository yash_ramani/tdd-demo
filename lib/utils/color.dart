import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Color black = Colors.black;
const Color transBlack = Colors.black38;
const Color white = Colors.white;

const Color primaryColor = Color(0xffEC8C26);

const Color systemRed = Color(0xffFF473D);
const Color iconsTextColor = Color(0xff3E424E);

const List<Color> customShapeGradiant = [
  Color(0xffED8D21),
  Color(0xffC3282E),
];
const Color customShapeShadow = Color(0x00000029);
const Color lightText = Color(0xff606060);
const Color reqTxtColor = Color(0xffC3282E);
const Color reqCountColor = Color(0xff202020);
const Color descTxtColor = Color(0xff2C2E39);
const Color reqLabelsColors = Color(0xff141414);
const Color newStatusColors = Color(0xff0C9E54);
const Color viewStatusColors = Color(0xffC3282E);
const Color respondStatusColors = Color(0xff0284D6);

const Color textFieldBorder = Color(0xffD5D6D6);
const Color hintColor = textFieldBorder;

const Color borderColor = Color(0xffB7BDC4);

const Color chatTileColor = Color(0xffEBEBEB);
